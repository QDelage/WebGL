# Système planétaire en ThreeJS

[![Codeship Status for QDelage/WebGL](https://app.codeship.com/projects/7f73a2af-094c-4fc8-9958-153103465c16/status?branch=master)](https://app.codeship.com/projects/424775)

Système planétaire en WebGL à l'aide de la librairie ThreeJS.

## Introduction

Ce dépôt est pour un projet universitaire de L3 Informatique. Il nous était demandé, en groupe, de créer un environnement spatial montrant un système extra-solaire, contenant des objets astronomiques avec des textures, et de leur appliquer des petits shaders pour modifier leur texture.

## Développé avec

* [ThreeJS](https://threejs.org/) - Bibliothèque JavaScript pour créer des scènes 3D
* [OrbitControls](https://riptutorial.com/three-js/example/26556/orbit-controls) - Caméra orbitale
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Auteurs

* **Quentin Delage** - *Développement JS* - [QDelage](https://gitlab.com/QDelage)
* **Paul Pont (profil anonymisé)** - *Développement shaders*

## License
Ce projet est disponible sous la license MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails

## Remerciements

* **layhwang** pour le [Soleil](https://clara.io/view/81031a5d-4788-42fd-b9f6-f63651653188#)
* **Elijah Rain (tyrosmith)** pour la [planète alienne](https://free3d.com/3d-model/alien-plantet-1-lybathan-13394.html)
* **FILCOMET** pour la [lune](https://www.turbosquid.com/3d-models/free-obj-model-moon-face/928585)
* **Gerhald3D** pour la [planète Mars](https://www.turbosquid.com/3d-models/realistic-mars-photorealistic-2k-3d-1277433)
* **scbcouch** pour la [planète sombre](https://clara.io/view/6ebb7144-1783-48cf-92a2-4eb0e7063ad7)